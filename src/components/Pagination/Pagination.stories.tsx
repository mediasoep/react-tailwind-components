import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Pagination } from "./Pagination";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Pagination",
  component: Pagination,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
} as ComponentMeta<typeof Pagination>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof Pagination.PageNumbers> = (args) => (
  <Pagination>
    <Pagination.PreviousPageButton />
    <Pagination.PageNumbers
      onClick={(event, page) => {
        console.log(event, page);
      }}
      {...args}
    />
    <Pagination.NextPageButton
      onClick={(event) => {
        console.log(event);
      }}
    />
  </Pagination>
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  currentPageNumber: 5,
  totalPageCount: 10,
  siblingCount: 1,
};
