import React, { useMemo } from "react";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/solid";

interface IUsePaginationProps {
  /**
   * The current page number (1-based index).
   */
  currentPageNumber: number;
  /**
   * The total number of pages.
   */
  totalPageCount: number;
  /**
   * The minimal number of page buttons to be shown on each side of the current page button. Defaults to 1.
   */
  siblingCount?: number;
}

interface IPaginationProps {
  children?: React.ReactNode;
  ariaLabel?: string;
}

interface IPaginatitionNumbersProp extends IUsePaginationProps {
  renderDots?: React.FC;
  renderNumber?: React.FC;
  onClick?: (
    event: React.MouseEvent<HTMLButtonElement>,
    pageNumber: string | number
  ) => void;
}

interface IPaginationButtonProps {
  renderButton?: React.FC;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  nextLabel?: string;
  previousLabel?: string;
}

const range = (start: number, end: number): number[] => {
  let length = end - start + 1;
  /*
  	Create an array of certain length and set the elements within it from
    start value to end value.
  */
  return Array.from({ length }, (_, idx) => idx + start);
};

export const usePagination = ({
  currentPageNumber,
  totalPageCount,
  siblingCount = 1,
}: IUsePaginationProps): (string | number)[] => {
  const paginationRange = useMemo((): (string | number)[] => {
    // Pages count is determined as siblingCount + firstPage + lastPage + currentPageNumber + 2*DOTS
    const totalPageNumbers = siblingCount + 5;

    /*
      Case 1:
      If the number of pages is less than the page numbers we want to show in our
      paginationComponent, we return the range [1..totalPageCount]
    */
    if (totalPageNumbers >= totalPageCount) {
      return range(1, totalPageCount);
    }

    /*
    	Calculate left and right sibling index and make sure they are within range 1 and totalPageCount
    */
    const leftSiblingIndex = Math.max(currentPageNumber - siblingCount, 1);
    const rightSiblingIndex = Math.min(
      currentPageNumber + siblingCount,
      totalPageCount
    );

    /*
      We do not show dots just when there is just one page number to be inserted between the extremes of sibling and the page limits i.e 1 and totalPageCount. Hence we are using leftSiblingIndex > 2 and rightSiblingIndex < totalPageCount - 2
    */
    const shouldShowLeftDots = leftSiblingIndex > 2;
    const shouldShowRightDots = rightSiblingIndex < totalPageCount - 2;

    const firstPageIndex = 1;
    const lastPageIndex = totalPageCount;

    /*
    	Case 2: No left dots to show, but rights dots to be shown
    */
    if (!shouldShowLeftDots && shouldShowRightDots) {
      let leftItemCount = 3 + 2 * siblingCount;
      let leftRange = range(1, leftItemCount);

      return [...leftRange, Pagination.DOTS, totalPageCount];
    }

    /*
    	Case 3: No right dots to show, but left dots to be shown
    */
    if (shouldShowLeftDots && !shouldShowRightDots) {
      let rightItemCount = 3 + 2 * siblingCount;
      let rightRange = range(
        totalPageCount - rightItemCount + 1,
        totalPageCount
      );
      return [firstPageIndex, Pagination.DOTS, ...rightRange];
    }

    /*
    	Case 4: Both left and right dots to be shown
    */
    if (shouldShowLeftDots && shouldShowRightDots) {
      let middleRange = range(leftSiblingIndex, rightSiblingIndex);
      return [
        firstPageIndex,
        Pagination.DOTS,
        ...middleRange,
        Pagination.DOTS,
        lastPageIndex,
      ];
    }

    return [];
  }, [totalPageCount, siblingCount, currentPageNumber]);

  return paginationRange;
};

export const Pagination = ({
  children,
  ariaLabel = "Pagination",
  ...props
}: IPaginationProps) => {
  return (
    <div className="text-center">
      <nav
        className="relative z-0 mt-8 inline-flex -space-x-px rounded-md shadow-sm"
        aria-label={ariaLabel}
        {...props}
      >
        {children}
      </nav>
    </div>
  );
};

Pagination.PreviousPageButton = (props: IPaginationButtonProps) => {
  const { renderButton, onClick, previousLabel = "Previous", ...rest } = props;

  return renderButton ? (
    renderButton(props)
  ) : (
    <button
      className="relative inline-flex items-center rounded-l-md border border-gray-300 bg-white px-2 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50"
      onClick={(e) => onClick?.(e)}
      {...rest}
    >
      <ChevronLeftIcon className="h-5 w-5 text-primary" aria-hidden="true" />
      <span className="sr-only">{previousLabel}</span>
    </button>
  );
};

Pagination.NextPageButton = (props: IPaginationButtonProps) => {
  const { renderButton, onClick, nextLabel = "Next", ...rest } = props;

  return renderButton ? (
    renderButton(props)
  ) : (
    <button
      className="relative inline-flex items-center rounded-r-md border border-gray-300 bg-white px-2 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50"
      onClick={(e) => onClick?.(e)}
      {...rest}
    >
      <span className="sr-only">{nextLabel}</span>
      <ChevronRightIcon className="h-5 w-5 text-primary" aria-hidden="true" />
    </button>
  );
};

Pagination.PageNumbers = (props: IPaginatitionNumbersProp) => {
  const {
    currentPageNumber,
    totalPageCount,
    siblingCount = 1,
    renderDots,
    renderNumber,
    onClick,
  } = props;
  const paginationRange = usePagination({
    currentPageNumber,
    totalPageCount,
    siblingCount,
  });

  return (
    <>
      {paginationRange.map((pageNumber, index) => {
        if (pageNumber === Pagination.DOTS) {
          return renderDots ? (
            renderDots(props)
          ) : (
            <span
              key={index}
              className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700"
            >
              ...
            </span>
          );
        }

        return renderNumber ? (
          renderNumber(props)
        ) : (
          <button
            key={index}
            className={`${
              pageNumber === currentPageNumber
                ? "z-10 border-primary bg-indigo-50 text-primary"
                : "border-gray-300 bg-white text-gray-500 hover:bg-gray-50"
            } relative inline-flex items-center border px-4 py-2 text-sm font-medium hover:no-underline`}
            aria-label={`Go to page ${pageNumber}`}
            onClick={(e) => onClick?.(e, pageNumber)}
          >
            {pageNumber}
          </button>
        );
      })}
    </>
  );
};

Pagination.DOTS = "...";
