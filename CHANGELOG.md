# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.0.5] - 2022-05-25

### Fixed
- Fix DOM elements error with labels on pagination buttons.

## [0.0.4] - 2022-05-25

### Fixed
- Add build files.

## [0.0.3] - 2022-05-25

### Added
- Add ability to translate pagination labels.

## [0.0.2] - 2022-05-21

### Fixed
- Fix nested exports.

## [0.0.1] - 2022-05-20

### Added
- Add Pagination component.
