import * as React from 'react';
import React__default, { useMemo } from 'react';

var tailwind = '';

function ChevronLeftIcon(props, svgRef) {
  return /*#__PURE__*/React.createElement("svg", Object.assign({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 20 20",
    fill: "currentColor",
    "aria-hidden": "true",
    ref: svgRef
  }, props), /*#__PURE__*/React.createElement("path", {
    fillRule: "evenodd",
    d: "M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z",
    clipRule: "evenodd"
  }));
}

const ForwardRef$1 = React.forwardRef(ChevronLeftIcon);
var ChevronLeftIcon$1 = ForwardRef$1;

function ChevronRightIcon(props, svgRef) {
  return /*#__PURE__*/React.createElement("svg", Object.assign({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 20 20",
    fill: "currentColor",
    "aria-hidden": "true",
    ref: svgRef
  }, props), /*#__PURE__*/React.createElement("path", {
    fillRule: "evenodd",
    d: "M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z",
    clipRule: "evenodd"
  }));
}

const ForwardRef = React.forwardRef(ChevronRightIcon);
var ChevronRightIcon$1 = ForwardRef;

var jsxRuntime = {exports: {}};

var reactJsxRuntime_production_min = {};

/**
 * @license React
 * react-jsx-runtime.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var f=React__default,k=Symbol.for("react.element"),l=Symbol.for("react.fragment"),m=Object.prototype.hasOwnProperty,n=f.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED.ReactCurrentOwner,p={key:!0,ref:!0,__self:!0,__source:!0};
function q(c,a,g){var b,d={},e=null,h=null;void 0!==g&&(e=""+g);void 0!==a.key&&(e=""+a.key);void 0!==a.ref&&(h=a.ref);for(b in a)m.call(a,b)&&!p.hasOwnProperty(b)&&(d[b]=a[b]);if(c&&c.defaultProps)for(b in a=c.defaultProps,a)void 0===d[b]&&(d[b]=a[b]);return {$$typeof:k,type:c,key:e,ref:h,props:d,_owner:n.current}}reactJsxRuntime_production_min.Fragment=l;reactJsxRuntime_production_min.jsx=q;reactJsxRuntime_production_min.jsxs=q;

{
  jsxRuntime.exports = reactJsxRuntime_production_min;
}

const jsx = jsxRuntime.exports.jsx;
const jsxs = jsxRuntime.exports.jsxs;
const Fragment = jsxRuntime.exports.Fragment;

const range = (start, end) => {
  let length = end - start + 1;
  return Array.from({
    length
  }, (_, idx) => idx + start);
};
const usePagination = ({
  currentPageNumber,
  totalPageCount,
  siblingCount = 1
}) => {
  const paginationRange = useMemo(() => {
    const totalPageNumbers = siblingCount + 5;
    if (totalPageNumbers >= totalPageCount) {
      return range(1, totalPageCount);
    }
    const leftSiblingIndex = Math.max(currentPageNumber - siblingCount, 1);
    const rightSiblingIndex = Math.min(currentPageNumber + siblingCount, totalPageCount);
    const shouldShowLeftDots = leftSiblingIndex > 2;
    const shouldShowRightDots = rightSiblingIndex < totalPageCount - 2;
    const firstPageIndex = 1;
    const lastPageIndex = totalPageCount;
    if (!shouldShowLeftDots && shouldShowRightDots) {
      let leftItemCount = 3 + 2 * siblingCount;
      let leftRange = range(1, leftItemCount);
      return [...leftRange, Pagination.DOTS, totalPageCount];
    }
    if (shouldShowLeftDots && !shouldShowRightDots) {
      let rightItemCount = 3 + 2 * siblingCount;
      let rightRange = range(totalPageCount - rightItemCount + 1, totalPageCount);
      return [firstPageIndex, Pagination.DOTS, ...rightRange];
    }
    if (shouldShowLeftDots && shouldShowRightDots) {
      let middleRange = range(leftSiblingIndex, rightSiblingIndex);
      return [firstPageIndex, Pagination.DOTS, ...middleRange, Pagination.DOTS, lastPageIndex];
    }
    return [];
  }, [totalPageCount, siblingCount, currentPageNumber]);
  return paginationRange;
};
const Pagination = ({
  children,
  ariaLabel = "Pagination",
  ...props
}) => {
  return /* @__PURE__ */ jsx("div", {
    className: "text-center",
    children: /* @__PURE__ */ jsx("nav", {
      className: "relative z-0 mt-8 inline-flex -space-x-px rounded-md shadow-sm",
      "aria-label": ariaLabel,
      ...props,
      children
    })
  });
};
Pagination.PreviousPageButton = (props) => {
  const {
    renderButton,
    onClick,
    previousLabel = "Previous",
    ...rest
  } = props;
  return renderButton ? renderButton(props) : /* @__PURE__ */ jsxs("button", {
    className: "relative inline-flex items-center rounded-l-md border border-gray-300 bg-white px-2 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50",
    onClick: (e) => onClick?.(e),
    ...rest,
    children: [/* @__PURE__ */ jsx(ChevronLeftIcon$1, {
      className: "h-5 w-5 text-primary",
      "aria-hidden": "true"
    }), /* @__PURE__ */ jsx("span", {
      className: "sr-only",
      children: previousLabel
    })]
  });
};
Pagination.NextPageButton = (props) => {
  const {
    renderButton,
    onClick,
    nextLabel = "Next",
    ...rest
  } = props;
  return renderButton ? renderButton(props) : /* @__PURE__ */ jsxs("button", {
    className: "relative inline-flex items-center rounded-r-md border border-gray-300 bg-white px-2 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50",
    onClick: (e) => onClick?.(e),
    ...rest,
    children: [/* @__PURE__ */ jsx("span", {
      className: "sr-only",
      children: nextLabel
    }), /* @__PURE__ */ jsx(ChevronRightIcon$1, {
      className: "h-5 w-5 text-primary",
      "aria-hidden": "true"
    })]
  });
};
Pagination.PageNumbers = (props) => {
  const {
    currentPageNumber,
    totalPageCount,
    siblingCount = 1,
    renderDots,
    renderNumber,
    onClick
  } = props;
  const paginationRange = usePagination({
    currentPageNumber,
    totalPageCount,
    siblingCount
  });
  return /* @__PURE__ */ jsx(Fragment, {
    children: paginationRange.map((pageNumber, index) => {
      if (pageNumber === Pagination.DOTS) {
        return renderDots ? renderDots(props) : /* @__PURE__ */ jsx("span", {
          className: "relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700",
          children: "..."
        }, index);
      }
      return renderNumber ? renderNumber(props) : /* @__PURE__ */ jsx("button", {
        className: `${pageNumber === currentPageNumber ? "z-10 border-primary bg-indigo-50 text-primary" : "border-gray-300 bg-white text-gray-500 hover:bg-gray-50"} relative inline-flex items-center border px-4 py-2 text-sm font-medium hover:no-underline`,
        "aria-label": `Go to page ${pageNumber}`,
        onClick: (e) => onClick?.(e, pageNumber),
        children: pageNumber
      }, index);
    })
  });
};
Pagination.DOTS = "...";

export { Pagination, usePagination };
//# sourceMappingURL=index.es.js.map
