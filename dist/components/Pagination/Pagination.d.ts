import React from "react";
interface IUsePaginationProps {
    /**
     * The current page number (1-based index).
     */
    currentPageNumber: number;
    /**
     * The total number of pages.
     */
    totalPageCount: number;
    /**
     * The minimal number of page buttons to be shown on each side of the current page button. Defaults to 1.
     */
    siblingCount?: number;
}
interface IPaginationProps {
    children?: React.ReactNode;
    ariaLabel?: string;
}
interface IPaginatitionNumbersProp extends IUsePaginationProps {
    renderDots?: React.FC;
    renderNumber?: React.FC;
    onClick?: (event: React.MouseEvent<HTMLButtonElement>, pageNumber: string | number) => void;
}
interface IPaginationButtonProps {
    renderButton?: React.FC;
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
    nextLabel?: string;
    previousLabel?: string;
}
export declare const usePagination: ({ currentPageNumber, totalPageCount, siblingCount, }: IUsePaginationProps) => (string | number)[];
export declare const Pagination: {
    ({ children, ariaLabel, ...props }: IPaginationProps): JSX.Element;
    PreviousPageButton(props: IPaginationButtonProps): JSX.Element | null;
    NextPageButton(props: IPaginationButtonProps): JSX.Element | null;
    PageNumbers(props: IPaginatitionNumbersProp): JSX.Element;
    DOTS: string;
};
export {};
